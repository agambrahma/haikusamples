
1.

> A drooping, shy leaf,
> Wind-blown, pulled in to the eye.
> The storm gathers all.

2.

> A new day is here.
> Away with your petty troubles.
> There’s a world out there.

3.

> Pen moves on paper,
> One mark follows another;
> The mind lags behind.

